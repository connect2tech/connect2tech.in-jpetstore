FROM ubuntu:18.04

MAINTAINER Naresh Chaurasia <message4naresh@gmail.com>

RUN apt update

RUN apt install -y openjdk-8-jdk

RUN apt install -y maven

RUN apt install -y git

RUN git clone https://bitbucket.org/connect2tech/connect2tech.in-jpetstore

WORKDIR /connect2tech.in-jpetstore

ENTRYPOINT ["/usr/bin/mvn","jetty:run"]